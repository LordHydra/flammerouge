package test;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import application.Carte;
import application.Rouleur;


import application.Joueur;
import application.Rouleur;
import application.Sprinter;


/**
 * @author Elouan
 *@author Vincent
 */
class JoueurTest {

	@Test
	void testConstructor() {
		Rouleur r = new Rouleur(1, 1);
		Sprinter s = new Sprinter(2, 1);
		Joueur j = new Joueur(1, "Test", s, r);
		assertEquals("L'id devrait �tre id",1,j.getID());
		assertEquals("Le nom devrait etre Test", "Test", j.getNom());
		assertEquals("Le rouleur devrait r", r, j.getRouleur());
		assertEquals("Le Sprinter devrait �tre s", s, j.getSprinter());
	}

}
