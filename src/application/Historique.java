package application;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Vincent
 * @author Elouan
 *
 */
public class Historique {
	private ArrayList<String[]> ensembleDesStatusParcours;
	private int nbJ;
	private ArrayList<Carte> ListCarteJoueParJoueur;
	
	
	/**
	 * Constructeur de la class Historique, initialise les attributs 
	 * ensembleDesStatusParcours, nbJ et ListCarteJoueParJoueur
	 *
	 */
	public Historique() {
		this.ensembleDesStatusParcours = new ArrayList<String[]>();
		this.ListCarteJoueParJoueur = new ArrayList<Carte>();
	}
	
	/**
	 *Methode permettant d'enregistrer dans un fichier texte le classement de joueurs
	 *@param lc
	 *			liste des pions sur le plateau
	 *
	 *@param lj
	 *			liste des joueurs en train de jouer
	 *
	 */
	public void makeScoreBoard(ArrayList<Cycliste> lc, ArrayList<Joueur> lj) {
		try {
			BufferedWriter fw = new BufferedWriter(new FileWriter("ScoreBoard.txt",true));
			String format = "dd/MM/yy H:mm:ss"; 
			java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat(format); 
			java.util.Date date = new java.util.Date(); 
			fw.newLine();
			fw.write("=========="+"Partie du :"+formater.format(date)+"==========");
			fw.newLine();
			fw.write("Les participants �taient :");
			fw.newLine();
			String gagnant="";
			for (int i=0;i<lj.size();i++) {
				fw.write("  -Joueur :"+lj.get(i).getID()+" "+lj.get(i).getNom());
				fw.newLine();
				if (lj.get(i).getID() == lc.get(0).getIdJoueur()) {
					gagnant = "Le gagnant de la course est "+lj.get(i).getNom();
				}
			}
			fw.write(gagnant);
			fw.newLine();
			fw.newLine();
			fw.write("Voici le tableau des scores :");
			fw.newLine();
			for (int i=0;i<lc.size();i++) {
				fw.write(" -Place :"+(i+1)+" "+lc.get(i).quelestMonType()+lc.get(i).getIdJoueur()+" � la case "+lc.get(i).getPosition());
				fw.newLine();
			}
			fw.write("================================================");
			fw.newLine();
			fw.close();
		}catch(IOException e) {
			System.out.println("Erreur lors de l'�criture");
		}
	}
	
	public void addStatusParcours(String[] stat) {
		this.ensembleDesStatusParcours.add(stat);
	}
	
	public void addCarte(Carte c) {
		this.ListCarteJoueParJoueur.add(c);
	}
	
	public void setNbJ(int nb) {
		this.nbJ=nb;
	}
	
	
	/**
	 *Methode permettant d'enregistrer les parties jouees dans un fichier textes
	 *@param lc
	 *			liste des pions sur le plateau
	 *
	 */
	public void makeLogs(ArrayList<Cycliste> lc) {
		try {
			BufferedWriter fw = new BufferedWriter(new FileWriter("Logs.txt",true));
			String format = "dd/MM/yy H:mm:ss"; 
			java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat(format); 
			java.util.Date date = new java.util.Date(); 
			fw.newLine();
			fw.write("################"+"Partie du :"+formater.format(date)+"################");
			fw.newLine();
			for (int i=0;i<this.ensembleDesStatusParcours.size();i++) {
				fw.write("Tour :"+(i+1));
				fw.newLine();
				for (int j=0;j<this.ensembleDesStatusParcours.get(i).length;j++) {
					String ligne = this.ensembleDesStatusParcours.get(i)[j];
					fw.write(ligne);
					fw.newLine();
				}
				fw.newLine();
			}
			for (int i=0;i<lc.size();i++) {
				ArrayList<Carte> listCartes = lc.get(i).getCartesJouees();
				fw.write("Le Joueur :"+lc.get(i).getIdJoueur()+" a jou� pour son "+lc.get(i).quelestMonType()+" les cartes suivantes :");
				fw.newLine();
				for (int j=0;j<listCartes.size();j++) {
					fw.write(listCartes.get(j).toString());
					fw.newLine();
				}
				fw.newLine();
			}
			fw.write("#############################################################");
			fw.newLine();
			fw.close();
		}catch (IOException e) {
			System.out.println("Erreur dans l'�criture du fichier");
		}
	}
}
