 	package application;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author Elouan
 *@author Vincent
 */
public class SaveParcours implements Serializable{
	private static final long serialVerionUID = 5118942035708796967L;
	private String nomFichier;
	
	
	/**
	 * Constructeur de la class SaveParcours
	 * @param NomFichier
	 * 					Nom du fichier dans lequel enregistrer les donn�es
	 */
	public SaveParcours(String NomFichier) {
		this.nomFichier=NomFichier;
	}
	
	/**
	 *Methode qui permet d'enregistrer un parcours dans un fichier binaire
	 *@param p
	 *			parcours � enregistrer
	 *
	 */
	public void sauve(Parcours p) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.nomFichier));
			oos.writeObject(p);
			oos.close();
		} catch (IOException e) {
			System.out.println("erreur d�E/S");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("erreur hors E/S");
			e.printStackTrace();
		}
	}
	
	
	public void saveAll() {
		SaveParcours sp = new SaveParcours("Parcours/Montagne");
		sp.sauve(this.p_LaHautMontagne());
		SaveParcours sp2 = new SaveParcours("Parcours/LaClassicissima");
		sp2.sauve(this.p_LaClassicissima());
	}

	
	/**
	 *Methode qui permet de r�cup�rer un parcours enregistrer dans un fichier binaire
	 *
	 *@return parcours r�cup�r�
	 */

	public Parcours charge() {
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.nomFichier));
			Parcours p = (Parcours) (ois.readObject());
			ois.close();
			return p;
		} catch (IOException e) {
			System.out.println("erreur d�E/S");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("erreur hors E/S");
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 *Methode qui g�n�re un parcours de type "Montagneux"
	 *
	 *@return parcours g�n�r�
	 *
	 */
	public Parcours p_LaHautMontagne() {
		Parcours p = new Parcours(69,"LaHautMontagne");
		p.addVirage(true, 21, 2);
		p.addVirage(true,28,2);
		p.addVirage(false, 30, 4);
		p.addVirage(true, 34, 2);
		p.addVirage(false, 41, 6);
		p.addVirage(true,46, 4);
		p.addVirage(false,55,4);
		
		p.addDune(32, true, 6, 5);
		p.addRelief(54, 11, 'M');
		return p;
	}
	
	/**
	 *Methode qui g�n�re un parcours de type "Classicissima"
	 *
	 *@return parcours g�n�r�
	 *
	 */
	public Parcours p_LaClassicissima() {
		Parcours p = new Parcours(69,"LaClassicissima");
		p.addVirage(true, 6, 2);
		p.addVirage(false,13,4);
		p.addVirage(false, 22, 2);
		p.addVirage(true, 24, 2);
		p.addVirage(true, 31, 4);
		p.addVirage(false, 35, 2);
		p.addVirage(false,47, 2);
		p.addVirage(true,54,4);
		p.addVirage(false,63,2);
		
		p.addDune(13, true, 9, 4);
		p.addDune(37, true, 4, 6);
		p.addDune(49, true, 2, 3);
		
		return p;
	}

}
