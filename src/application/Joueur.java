package application;

/**
 * @author Vincent
 * @author Elouan
 *
 */
public class Joueur {
	private int idJoueur;
	private String nom;
	private Sprinter sprinter;
	private Rouleur rouleur;
	
	
	/**
	 * Constructeur de la class Joueur
	 * @param id, n, s, r
	 * 			valeur de l'idJouer
	 * @param n
	 * 			nom du joueur
	 * @param s
	 * 			le pion sprinter du joueur
	 * @param r
	 * 			le pion rouleur du joueur
	 *
	 */
	public Joueur(int id, String n, Sprinter s, Rouleur r){
		this.idJoueur=id;
		this.nom = n;
		this.sprinter = s;
		this.rouleur = r;
	}
	
	
	/**
	 * @return le nom du joueur
	 *
	 */
	public String getNom(){
		return this.nom;
	}
	
	/**
	 * @return le nom du joueur
	 *
	 */
	public int getID(){
		return this.idJoueur;
	}
	
	/**
	 * @return le pion sprinter du joueur
	 *
	 */
	public Sprinter getSprinter(){
		return this.sprinter;
	}
	
	
	/**
	 * @return le pion rouleur du joueur
	 *
	 */
	public Rouleur getRouleur(){
		return this.rouleur;
	}
	
	
	/**
	 * @return une cha�ne de caractere contenant des informations sur le joueur
	 *
	 */
	public String toString(){
		return "je suis le joueur " + this.idJoueur +"\n"+this.rouleur.toString()+"\n"+this.sprinter.toString();
	}
	
	
}
