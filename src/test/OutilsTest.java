package test;

import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

import application.Cycliste;
import application.Outils;
import application.Sprinter;

/**
 * @author Elouan
 *@author Vincent
 */
class OutilsTest {

	@Test
	void testTrierListCycliste() {
		Sprinter s1 = new Sprinter(1, 1);
		Sprinter s2 = new Sprinter(3, 1);
		Sprinter s3 = new Sprinter(2, 1);
		ArrayList<Cycliste> l = new ArrayList<Cycliste>();
		l.add(s1);
		l.add(s2);
		l.add(s3);
		ArrayList<Cycliste> lt = Outils.trierListCycliste(l);
		assertEquals("Devrait �tre s2", s2, lt.get(0));
		assertEquals("Devrait �tre s3", s3, lt.get(1));
		assertEquals("Devrait �tre s2", s1, lt.get(2));
	}

	
	
	@Test
	void testGroupesCycliste() {
		Sprinter s1 = new Sprinter(1, 1);
		Sprinter s2 = new Sprinter(2, 1);
		Sprinter s3 = new Sprinter(4, 1);
		Sprinter s4 = new Sprinter(5, 1);
		ArrayList<Cycliste> l = new ArrayList<Cycliste>();
		l.add(s1);
		l.add(s2);
		l.add(s3);
		l.add(s4);
		ArrayList<Cycliste> lt = Outils.trierListCycliste(l);
		ArrayList<ArrayList<Cycliste>> groupes = Outils.groupesCycliste(lt);
		assertEquals("Devrait �tre �gal � 2", 2, groupes.size());
	}
}
