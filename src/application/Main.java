package application;

import java.util.*;

/**
 * @author Vincent
 * @author Elouan
 *
 */

public class Main {
	
	public static void main(String[] args) {
		System.out.println("Flamme Rouge est un jeu de course cycliste rapide et tactique");
		System.out.println("dans lequel chaque joueur contr�le une �quipe de deux coureurs :");
		System.out.println("un rouleur et un sprinteur. Les joueurs d�placent leurs coureurs en");
		System.out.println("jouant des cartes qui indiquent de combien de cases ils avancent.");
		System.out.println("Le but du jeu est d��tre le premier joueur � franchir la ligne d�arriv�e");
		System.out.println("avec un des deux coureurs. ");
		
		System.out.println("");
		System.out.println("Durant votre partie vous aurez l'affichage du parcours du jeu de la fa�on suivante"+"\n");
		Parcours p = new Parcours(42,"tuto");
		p.addVirage(true, 15, 2);
		p.addVirage(false, 20, 2);
		p.addDune(20, true, 7, 8);
		Outils.afficherTerrain(p);
		System.out.println("");
		System.out.println("|==|               |DP|     |AR|                  |VL|    |VS|                   |  |    |  |                 ");
		System.out.println("|  | est une case  |  |  et |  | sont les cases   |  | et |  | sont des virages  |  | et |  | sont des mont�es");
		System.out.println("|  |   classique   |  |     |  | d'arriv�e et de  |  |    |  |  serr� ou l�ger   |  |    |  |  ou des virages ");
		System.out.println("|==|    (plate)    |DP|     |AR|     depart       |  |    |  |                   |MO|    |DE|                 ");
		System.out.println("");
		System.out.println("Vous serez en possession d'un sprinter et d'un rouleur qui chacun aura son Deck de cartes energie");
		System.out.println("Une carte energie, est une carte qui possede une valeur, cette valeur correspond au nombres de case que le cycliste va avancer");
		System.out.println("Le jeu poss�de 3phase par tour, la premi�re chaque joueur selectionne une carte dans le deck de leur rouleur,ensuitre de leur sprinter");
		System.out.println("ou inversement. la seconde phase est simplement la phase de mouvement, dans laquelle on prend les cartes selectionn�es par les joueurs");
		System.out.println("et on fait avancer les Cycliste correspondant de la valeur de la carte choisi, la phase final permet d'appliquer le ph�nom�ne de fatigue");
		System.out.println("ainsi que le ph�nom�ne d'aspiration");
		System.out.println("");
		System.out.println("Vous avez le choix entre deux circuits La Haute Montagne ou La Classicissima");
		System.out.println("Lequel voulez vous jouer ? (tapez montagne ou classicissima)");
		Scanner s = new Scanner(System.in);
		String choix = s.next();
		Parcours p1;
		while (!choix.equals("montagne") && !choix.equals("classicissima")) {
			System.out.println("");
			System.out.println("Vous devez choisir entre montagne ou classicissima");
			System.out.println("Vous avez le choix entre deux circuits La Haute Montagne ou La Classicissima");
			System.out.println("Lequel voulez vous jouer ? (tapez montagne ou classicissima)");
			choix = s.next();
		}
		if (choix.equals("montagne")) {
			SaveParcours sp = new SaveParcours("Parcours/Montagne");
			p1 = sp.p_LaHautMontagne();
		}else {
			SaveParcours sp2 = new SaveParcours("Parcours/LaClassicissima");
			p1 = sp2.p_LaClassicissima();
		}
		Jeu j = new Jeu(p1);
		j.startGame();
		
		
		
	}
	
 }
