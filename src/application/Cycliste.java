package application;
import java.util.ArrayList;
import java.util.Collections;


/**
 * @author Vincent
 * @author Elouan
 *
 */
public abstract class Cycliste {	
	private int position;
	private boolean estAdroite;
	private int idjoueur;
	protected ArrayList<Carte> paquetDeCartes;
	protected ArrayList<Carte> paquetCartesVisibles;
	protected Carte carteJoue;
	private ArrayList<Carte> cartesJouees;
	
	/**
	 * Constructeur de la class Cycliste
	 * @param p
	 * 			donne la position du pion cycliste
	 * 
	 * @param idjoueur
	 * 			correspond au joueur � qui appartient ce cycliste
	 *
	 *	Initialise les autres attributs par d�faut. 
	 */
	public Cycliste(int p,int idjoueur){
		this.position = p;
		this.idjoueur=idjoueur;
		this.paquetDeCartes = new ArrayList<Carte>();
		this.paquetCartesVisibles = new ArrayList<Carte>();
		this.carteJoue=null;
		this.cartesJouees = new ArrayList<Carte>();
		
	}
	
	/**
	 * Methode abstract qui remplit l'attribut Deck de class
	 *
	 */
	public abstract void remplirDeck();
	
	/**
	 * Methode abstract
	 * @return une cha�ne de caract�re donnant le type de l'instance
	 *
	 */
	public abstract String quelestMonType();
	
	
	/**
	 * Methode permettant au pion cycliste d'avancer sur le circuit
	 * @param mv
	 * 			Le nombre de cases que le cycliste doit avancer
	 */
	public void avancer(int mv){
		this.position += mv;
	}
	
	/**
	 * @return la position du pion cycliste
	 *
	 */
	public int getPosition(){
		return this.position;
	}
	
	public void setEstDroite(boolean d) {
		this.estAdroite=d;
	}
	
	public void setCarteJouer(Carte c) {
		this.carteJoue=c;
	}
	
	public Carte getCarteJouer() {
		return this.carteJoue;
	}
	
	public ArrayList<Carte> getCarteVisible() {
		return this.paquetCartesVisibles;
	}
	
	public boolean getEstAdroite() {
		return this.estAdroite;
	}
	
	public int getIdJoueur() {
		return this.idjoueur;
	}
	
	public ArrayList<Carte> getCartesJouees() {
		return cartesJouees;
	}
	
	
	/**
	 * Methode permettant de piocher dans l'attribut Deck de la class
	 * @return une liste de Carte contenant la pioche
	 *
	 */
	public ArrayList<Carte> piocher(){
		try {
			ArrayList<Carte> res = new ArrayList<Carte>();
			if(this.paquetDeCartes.size() < 4){
				while(!this.paquetDeCartes.isEmpty()){
					res.add(this.paquetDeCartes.get(0));
					this.paquetDeCartes.remove(0);
				}
				while(!paquetCartesVisibles.isEmpty()) {
					this.paquetDeCartes.add(paquetCartesVisibles.get(0));
					paquetCartesVisibles.remove(0);
				}
				Collections.shuffle(this.paquetDeCartes);
				while(res.size() != 4) {
					res.add(this.paquetDeCartes.get(0));
					this.paquetDeCartes.remove(0);
				}
			}
			else{
				for(int i = 0; i < 4; i++){
					res.add(this.paquetDeCartes.get(0));
					this.paquetDeCartes.remove(0);
				}
			}
			return res;			
		}catch (Exception e) {
			System.out.println("Pioche de secours");
			ArrayList<Carte> res = new ArrayList<Carte>();
			res.add(new CarteFatigue());
			res.add(new CarteFatigue());
			res.add(new CarteFatigue());
			res.add(new CarteFatigue());
			return res;
		}
	}
	
	public void ajouterCarte(Carte c) {
		this.paquetCartesVisibles.add(c);
	}
	
	
	/**
	 * Methode permettant de choisir quelle carte on souhaite jouer entre les 4 cartes que nous avions piocher
	 * @param pioche
	 * 				La liste de carte contenant la pioche
	 * @param i		
	 * 			L'index de la carte que nous choisissons de jouer
	 * 
	 *  
	 * @return la Carte que nous avons choisi de jouer
	 *
	 */
	public Carte choixCarteAJouer(ArrayList<Carte> pioche, int i){
		Carte res = pioche.get(i);
		ArrayList<Carte> cartesADef = pioche;
		cartesADef.remove(i);
		while(!cartesADef.isEmpty()){
			this.paquetCartesVisibles.add(cartesADef.get(0));
			cartesADef.remove(0);
		}
		this.cartesJouees.add(res);
		return res;
	}
	
	
	/**
	 * Methode qui permet d'afficher la liste des Cartes (et leurs valeurs) contenues dans l'attribut Deck de la class
	 * 
	 *  @param p
	 *  	ce sera la liste plac� en param�tre qui sera affich�
	 *
	 */
	public void afficherPaquetDeCartes(ArrayList<Carte> p){
		String res = "";
		for(int i = 0; i < p.size(); i++){
			res = res + "Carte " + (i+1) + ": " + p.get(i) + "\n";
		}
		System.out.println(res);
	}
	
	public ArrayList<Carte> getPaquetDeCartes(){
		return this.paquetDeCartes;
	}
	
	public String toString() {
		return "j'appartiens au joueur"+this.idjoueur+" Je suis en position "+this.position+" ma carte stock�e est de valeur"+this.carteJoue.getValeur();
	}
}
