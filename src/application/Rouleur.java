package application;
import java.util.Collections;

/**
 * @author Elouan
 * @author Vincent
 *
 */
public class Rouleur extends Cycliste{
	
	
	/**
	 * Constructeur de la classe Rouleur
	 * @param p
	 * 			Valeur de la position du pion Rouleur
	 * @param idjoueur
	 * 		joueur a qui appartient le rouleur
	 */
	public Rouleur(int p,int idjoueur){
		super(p,idjoueur);
	}
	
	
	/**
	 * Methode permettant de remplir l'attribut Deck de l'instance
	 *
	 */
	public void remplirDeck(){
		int[] tab = {3,3,3,4,4,4,5,5,5,6,6,6,7,7,7};		
		for(int j = 0; j < 15; j ++){
			this.paquetDeCartes.add(new CarteEnergie(tab[j]));
		}
		Collections.shuffle(this.paquetDeCartes);
	}
	
	
	/**
	 * @return une cha�ne de caract�res donnant l'insformation sur le type de l'instance
	 *
	 */
	public String quelestMonType() {
		return "R";
	}
	
	
	/**
	 * @return une cha�ne de caract�res donnant des informations sur l'instance
	 *
	 */
	public String toString(){
		String res = super.toString();
		res +=" Je suis un rouleur";
		return res;
	}
}
