package test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import application.Carte;
import application.Rouleur;

/**
 * @author Elouan
 *@author Vincent
 */
class RouleurTest {

	@Test
	void testConstructor() {
		Rouleur r = new Rouleur(2, 3);
		assertEquals("Devrait se trouver en position 2", 2, r.getPosition());
		assertEquals("Devrait appartenir au joueur 3", 3, r.getIdJoueur());
	}
	
	@Test
	void testRemplirDeck() {
		Rouleur r = new Rouleur(2, 3);
		r.remplirDeck();
		ArrayList<Carte> deck = r.getPaquetDeCartes();
		int nb3 = 0;
		int nb4 = 0;
		int nb5 = 0;
		int nb6 = 0;
		int nb7 = 0;
		for(int i = 0; i < deck.size(); i++) {
			if(deck.get(i).getValeur() == 3){
				nb3 += 1;
			}
			if(deck.get(i).getValeur() == 4){
				nb4 += 1;
			}
			if(deck.get(i).getValeur() == 5){
				nb5 += 1;
			}
			if(deck.get(i).getValeur() == 6){
				nb6 += 1;
			}
			if(deck.get(i).getValeur() == 7){
				nb7 += 1;
			}
		}
		assertEquals("Devrait �tre �gal � 3", 3, nb3);
		assertEquals("Devrait �tre �gal � 3", 3, nb4);
		assertEquals("Devrait �tre �gal � 3", 3, nb5);
		assertEquals("Devrait �tre �gal � 3", 3, nb6);
		assertEquals("Devrait �tre �gal � 3", 3, nb7);	
	}
	
	@Test
	public void testAvancer(){
		Rouleur r = new Rouleur(2, 3);
		r.avancer(1);
		assertEquals("Devrait �tre �gal � 3", 3, r.getPosition());
		r.avancer(4);
		assertEquals("Devrait �tre �gal � 7", 7, r.getPosition());
		
	}
	
	
	@Test
	public void testPiocher(){
		Rouleur r = new Rouleur(2, 3);
		r.remplirDeck();
		ArrayList<Carte> pioche = r.piocher();
		ArrayList<Carte> deck = r.getPaquetDeCartes();
		assertEquals("Devrait �tre �gal � 11", 11, deck.size());
		assertEquals("Devrait �tre �gal � 4", 4, pioche.size());	
	}
	
	
	@Test
	public void testCarteAJouer(){
		Rouleur r = new Rouleur(2, 3);
		r.remplirDeck();
		ArrayList<Carte> pioche = r.piocher();
		Carte[] tab = new Carte[4];
		for(int i = 0; i < 4; i++) {
			tab[i] = pioche.get(i);
		}
		Carte c = r.choixCarteAJouer(pioche, 2);
		assertEquals("Devrait correspondre � la valeur de la carte pioch�e", tab[2], c);
	}
	
	
	
	
	
	
	
}