package application;

/**
 * @author Elouan
 * @author Vincent
 */
public class CarteFatigue extends Carte {

		public final static int DEFAULT_VAL = 2;
		
		
		
		/**
		 * Constructeur de la classe CarteFatigue
		 *
		 */
		public CarteFatigue(){
			super(DEFAULT_VAL);
		}
		
		
		/**
		 * @return une cha�ne de caract�re donnant des informations sur la CarteFatigue
		 *
		 */
		public String toString(){
			return "Carte Fatigue de valeur: " + this.getValeur();
		}
}
