package application;

/**
 * @author Elouan
 * @author Vincent
 *
 */
/**
 * Class ParcoursException permettant de renvoyer un meesage en cas d'erreur
 *
 */
public class ParcoursException extends Exception {
	
	
	/**
	 * Constructeur de la class ParcoursException 
	 * @param s
	 * 		message qu'on souhaite afficher
	 *
	 */
	public ParcoursException(String s) {
		super(s);
	}
}
