package test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;
import application.CarteEnergie;

/**
 * @author Elouan
 *@author Vincent
 */
class CarteEnergieTest {
	@Test
	public void testConstructor() {
		CarteEnergie ce = new CarteEnergie(5);
		assertEquals("La carte devrait avoir 5 comme valeur",5,ce.getValeur());
	}

}
