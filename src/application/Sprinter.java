package application;
import java.util.Collections;

/**
 * @author Elouan
 *@author Vincent
 */
public class Sprinter extends Cycliste {
	
	/**
	 * Cosntructeur de la class Sprinter
	 * @param p 
	 * 			La position du pion sprinter
	 * @param idjoueur
	 * 		Joeur a qui appartient le sprinter
	 *
	 */
	public Sprinter(int p,int idjoueur){
		super(p,idjoueur);
	}
	
	/**
	 * Methode qui permet de remplir l'attribut Deck de l'instance
	 * 
	 */
	public void remplirDeck(){
		int[] tab = {2,2,2,3,3,3,4,4,4,5,5,5,9,9,9};
		for(int j = 0; j < 15; j ++){
			this.paquetDeCartes.add(new CarteEnergie(tab[j]));
		}
		Collections.shuffle(this.paquetDeCartes);
	}
	
	
	/**
	 * @return une cha�ne de caract�res donnant l'insformation sur le type de l'instance
	 *
	 */
	public String quelestMonType() {
		return "S";
	}
	
	
	/**
	 * @return une cha�ne de caract�res donnant des informations sur l'instance
	 *
	 */
	public String toString(){
		String res = super.toString();
		res +=" Je suis un sprinter";
		return res;
	}
}
