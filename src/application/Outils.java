package application;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;



/**
 * @author Vincent
 * @author Elouan
 *
 */
/**
 * Classe contenant une liste de m�thodes que nous avons besoin d'utiliser dans les autres classes
 *
 */
public class Outils {
	
	
	/**
	 * Methode permettant d'afficher la position des pions des joueurs dans la zone de d�part
	 * @param positionDepart
	 * 						Tableau � deux dimensions contenant les position des pions
	 *
	 */
	public static void afficherTab(String[][] positionDepart) {
		String place="",ligne1="",ligne2="";
		for(int i=0;i<positionDepart[0].length;i++) {
			place +=(i+1)+"  ";
			ligne1 +=positionDepart[0][i]+" ";
			ligne2 +=positionDepart[1][i]+" ";	
		}
		System.out.println("");
		System.out.println("Position "+place + "Depart");
		System.out.println("Gauche   "+ligne1 + "Depart");
		System.out.println("Droite   "+ligne2 + "Depart");
	}
	
	
	/**
	 * Methode permettant d'ins�rer la position d'un nouveau pion dans la zone de d�part
	 * @param positionDepart
	 *						Tableau � deux dimensions contenant les position des pions
	 *@param pos
	 *						Position du pion
	 *@param idjoueur
	 *						Id du joueur � qui appartient le pion
	 *@param p
	 *						Circuit dans lequel positionner le pion
	 *@param c
	 *						Cycliste (Rouleur ou Sprinter) qu'on veut positionner sur le plateau
	 *			
	 */
	public static void addCoordIntab(String[][] positionDepart,int pos,int idjoueur,Parcours p,Cycliste c) {
		Scanner sc = new Scanner(System.in);
		while(true) {
			try {
				while (!p.estDepart(pos-1)) {
					System.out.println("Vous avez le droit de placer votre cycliste uniquement sur les cases de d�part");
					System.out.println("Les cases d�part vont de la case 1 � la case 5");
					pos = sc.nextInt();
				}
				while(positionDepart[0][pos-1]!="NN" && positionDepart[1][pos-1]!="NN") {
					System.out.println("Position d�j� prise veuillez en donner une autre (une place avec un NN)");
					afficherTab(positionDepart);
					pos = sc.nextInt();
				}
				break;
			}catch (InputMismatchException e) {
				System.out.println("Veuillez entrer un chiffre et non une lettre svp !");
				sc.next();
			}			
		}
		if (positionDepart[1][pos-1]!="NN") {
			positionDepart[0][pos-1]=(c.quelestMonType()+idjoueur);
			c.setEstDroite(false);
		}else {
			positionDepart[1][pos-1]=(c.quelestMonType()+idjoueur);
			c.setEstDroite(true);
		}
	}
	
	
	/**
	 * M�thode permettant de savoir si le nom de Joueur existe d�j�
	 * 
	 * @param l
	 * 			Liste de joueurs d�ja existants
	 * 
	 * @param nom
	 * 			Nom du nouveau joueur � ins�rer dans la liste
	 * 
	 * @return boolean
	 * true si le nom � ins�rer existe d�j� dans la liste
	 * false si le nom � ins�rer n'existe pas dans la liste
	 *			
	 */	
	public static boolean nomExistant(ArrayList<Joueur> l,String nom) {
		Iterator<Joueur> i = l.iterator();
		while(i.hasNext()) {
			if (i.next().getNom().equals(nom)) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * M�thode permettant de s�lectionner une carte dans la main du joueur pour la jouer
	 * 
	 * @param c
	 * 			Cycliste (Rouleur ou Sprinter) dont avons besoin de piocher dans son Deck
	 * 
	 * @return la carte que l'utilisateur a choisi de jouer
	 *			
	 */	
	public static Carte selectCarteAjouerUtilsateur(Cycliste c) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Carte> main = c.piocher();
		System.out.println("===================Votre Main===================");
		c.afficherPaquetDeCartes(main);
		int choix;
		while(true) {
			try {
				System.out.println("Quelle carte souhaitez vous jouer (1ere � 4eme carte) ?");
				choix = sc.nextInt();
				while (choix<1||choix>4) {
					System.out.println("Il y a 4 cartes choisisez en une parmis les 4 svp");
					choix = sc.nextInt();
				}
				break;
			}catch (InputMismatchException e) {
				System.out.println("Veuillez entrer un chiffre et non une lettre svp !");
				sc.next();
			}			
		}
		Carte jouer = c.choixCarteAJouer(main, choix-1);
		System.out.println("================================================");
		return jouer;
	}


	/**
	 * M�thode permettant de tirer les cyclistes dans leur ordre d�croissant de positionnement
	 * 
	 * @param lc
	 * 			La liste de cyclistes sur le plateau
	 * 
	 * @return Une liste de listes de cyclistes class�s par ordre d�croissant en partant du 1er
	 *			
	 */	
	public static ArrayList<Cycliste> trierListCycliste(ArrayList<Cycliste> lc){
		ArrayList<Cycliste> res = lc;
		Collections.sort(res, new Comparator<Cycliste>(){
			@Override
			public int compare(Cycliste a,Cycliste b) {
				if (a.getPosition()>b.getPosition()) {
					return 1;
				}else if (a.getPosition()<b.getPosition()) {
					return -1;
				}else {
					if (a.getEstAdroite()) {
						return 1;
					}else {
						return -1;
					}
				}
			}
		});
		Collections.reverse(res);
		return res;
	}
	
	
	/**
	 * M�thode permettant de former les groupes de cyclistes sur le circuit
	 * 
	 * @param l
	 * 			La liste de cyclistes sur le circuit (class�s par ordre de placement)
	 * 
	 * @return Une liste de listes de cycliste (chaque liste de la liste retourn�e repr�sente un groupe de cyclistes)
	 *			
	 */	
	public static ArrayList<ArrayList<Cycliste>> groupesCycliste(ArrayList<Cycliste> l){
		ArrayList<ArrayList<Cycliste>> res = new ArrayList<ArrayList<Cycliste>>();
		ArrayList<Cycliste> copie_l = new ArrayList<Cycliste>(l);
		ArrayList<Cycliste> groupe = new ArrayList<Cycliste>();
		if (!copie_l.isEmpty()) {
			groupe.add(copie_l.get(0));
			copie_l.remove(0);
		}
		while(!copie_l.isEmpty()) {
			Cycliste c = copie_l.get(0);
			Cycliste c_prec = groupe.get(0);
			copie_l.remove(0);
			if (c.getPosition() == c_prec.getPosition() || (c_prec.getPosition() - c.getPosition()) == 1) {
				groupe.add(0,c);
			}else {				
				res.add(groupe);
				groupe = new ArrayList<Cycliste>();
				groupe.add(c);
			}
		}
		res.add(groupe);
		for (int i=0;i<res.size();i++) {
			Collections.reverse(res.get(i));
		}
	
		return res;
	}
	
	public static void aspiration() {
		
	}

	
	/**
	 * M�thode permettant d'afficher l'�tat du circuit et de la partie
	 * @param p
	 * 		Parcours : p ; parcours que l'on veut afficher
	 * @return
	 * 		Le tableau de chaine de carat�re repr�sentant le parcours
	 *			
	 */	
	public static String[] afficherTerrain(Parcours p) {
		int[] relief = p.getRelief();
		String[][] posJoueurs = p.getStatusParcours();
		String[] terrain = p.getTerrain();
		StringBuffer l1=new StringBuffer("|");
		StringBuffer l6=new StringBuffer("|");
		String l2="|",l3="|",l4="|",l5="|";
		for (int i=0;i<relief.length;i++) {
			l1.append("===");
			l6.append("===");
			switch(terrain[i]) {
			case "Depart":
				l2+="DP|";
				l5+="DP|";
				break;
			case "Arrive":
				l2+="AR|";
				l5+="AR|";
				break;
			case "VirageLeger":
				l2+="VL|";
				break;
			case "VirageSerre":
				l2+="VS|";
				break;
			default:
				l2+="==|";
				break;
			}
			if(relief[i]==1) {
				l5+="MO|";
			}else if (relief[i]==2) {
				l5+="DE|";
			}else{
				if (!(terrain[i].equals("Depart") || terrain[i].equals("Arrive"))){
					l5+="==|";
				}
			}
			switch(posJoueurs[0][i]) {
			case "R1":
				l3+="R1|";
				break;
			case "R2":
				l3+="R2|";
				break;
			case "R3":
				l3+="R3|";
				break;
			case "R4":
				l3+="R4|";
				break;
			case "S1":
				l3+="S1|";
				break;
			case "S2":
				l3+="S2|";
				break;
			case "S3":
				l3+="S3|";
				break;
			case "S4":
				l3+="S4|";
				break;
			default:
				l3+="  |";
				break;
			}
			
			switch(posJoueurs[1][i]) {
			case "R1":
				l4+="R1|";
				break;
			case "R2":
				l4+="R2|";
				break;
			case "R3":
				l4+="R3|";
				break;
			case "R4":
				l4+="R4|";
				break;
			case "S1":
				l4+="S1|";
				break;
			case "S2":
				l4+="S2|";
				break;
			case "S3":
				l4+="S3|";
				break;
			case "S4":
				l4+="S4|";
				break;
			default:
				l4+="  |";
				break;
			}
			
		}
		l1.deleteCharAt(l1.length()-1);
		l6.deleteCharAt(l6.length()-1);
		l1.append("|");
		l6.append("|");
		String[] res = {l1.toString(),l2,l3,l4,l5,l6.toString()};
		System.out.println("");
		System.out.println(l1+"\n"+l2+"\n"+l3+"\n"+l4+"\n"+l5+"\n"+l6);
		System.out.println("");
		return res;
	}
	
	//maquette
	/*
	|=======================================================================|
	|DP|DP|DP|DP|DP|==|==|==|VL|VL|==|==|==|==|==|VS|VS|==|==|AR|AR|AR|AR|AR|
	|  |  |  |  |  |  |  |  |  |R1|  |  |  |  |  |  |  |  |  |  |  |  |  |  |
	|  |  |  |  |  |  |  |  |S1|S2|  |R2|  |  |  |  |  |  |  |  |  |  |  |  |
	|DP|DP|DP|DP|DP|==|==|==|==|==|MO|MO|MO|MO|DE|DE|DE|DE|DE|AR|AR|AR|AR|AR|
	|=======================================================================|
	*/

}
