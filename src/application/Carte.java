package application;

/**
 * @author Elouan
 * @author Vincent
 */
public abstract class Carte {
	
	private int valeur;
	
	/**
	 * Constructeur de la class  Carte, initialise l'attribue valeur
	 * @param v
	 * 			La valeur de la Carte 
	 *
	 */
	public Carte(int v){
		this.valeur = v;
	}
	
	
	/**
	 * @return la valeur de la Carte
	 *
	 */
	public int getValeur(){
		return this.valeur;
	}
	
	
	
	/**
	 * Change la valeur de la Carte
	 * @param v
	 * 			Nouvelle valeur de la Carte
	 *
	 */
	public void setValeur(int v){
		this.valeur = v;
	}
	
	
	
	/**
	 * @return une cha�ne de caract�re donnant des informations sur la Carte
	 *
	 */
	public String toString(){
		return "Carte de valeur: " + this.getValeur();
	}

	
}
