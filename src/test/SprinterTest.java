package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import application.Carte;
import application.Sprinter;


/**
 * @author Elouan
 *@author Vincent
 */
class SprinterTest {

	
	@Test
	void testConstructor() {
		Sprinter s = new Sprinter(2, 3);
		assertEquals("Devrait se trouver en position 2", 2, s.getPosition());
		assertEquals("Devrait appartenir au joueur 3", 3, s.getIdJoueur());
	}
	
	@Test
	void testRemplirDeck() {
		Sprinter s = new Sprinter(2, 3);
		s.remplirDeck();
		ArrayList<Carte> deck = s.getPaquetDeCartes();
		int nb2 = 0;
		int nb3 = 0;
		int nb4 = 0;
		int nb5 = 0;
		int nb9 = 0;
		for(int i = 0; i < deck.size(); i++) {
			if(deck.get(i).getValeur() == 2){
				nb2 += 1;
			}
			if(deck.get(i).getValeur() == 3){
				nb3 += 1;
			}
			if(deck.get(i).getValeur() == 4){
				nb4 += 1;
			}
			if(deck.get(i).getValeur() == 5){
				nb5 += 1;
			}
			if(deck.get(i).getValeur() == 9){
				nb9 += 1;
			}
		}
		assertEquals("Devrait �tre �gal � 3", 3, nb2);
		assertEquals("Devrait �tre �gal � 3", 3, nb3);
		assertEquals("Devrait �tre �gal � 3", 3, nb4);
		assertEquals("Devrait �tre �gal � 3", 3, nb5);
		assertEquals("Devrait �tre �gal � 3", 3, nb9);	
	}
	
	@Test
	public void testAvancer(){
		Sprinter s = new Sprinter(2, 3);
		s.avancer(1);
		assertEquals("Devrait �tre �gal � 3", 3, s.getPosition());
		s.avancer(4);
		assertEquals("Devrait �tre �gal � 7", 7, s.getPosition());
		
	}
	
	
	@Test
	public void testPiocher(){
		Sprinter s = new Sprinter(2, 3);
		s.remplirDeck();
		ArrayList<Carte> pioche = s.piocher();
		ArrayList<Carte> deck = s.getPaquetDeCartes();
		assertEquals("Devrait �tre �gal � 11", 11, deck.size());
		assertEquals("Devrait �tre �gal � 4", 4, pioche.size());	
	}
	
	
	@Test
	public void testCarteAJouer(){
		Sprinter s = new Sprinter(2, 3);
		s.remplirDeck();
		ArrayList<Carte> pioche = s.piocher();
		Carte[] tab = new Carte[4];
		for(int i = 0; i < 4; i++) {
			tab[i] = pioche.get(i);
		}
		Carte c = s.choixCarteAJouer(pioche, 2);
		assertEquals("Devrait correspondre � la valeur de la carte pioch�e", tab[2], c);
	}

}
