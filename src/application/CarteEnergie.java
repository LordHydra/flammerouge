package application;

/**
 * @author Elouan
 * @author Vincent
 *
 */
public class CarteEnergie extends Carte{
	
	
	/**
	 * Constructeur de la classe CarteEnergie
	 * @param v
	 * 			Valeur de la CarteEnergie
	 *
	 */
	public CarteEnergie(int v){
		super(v);
	}
	
	
	/**
	 * @return une cha�ne de caract�re donnant des informations sur la CarteEnergie
	 *
	 */
	public String toString(){
		return "Carte Energie de valeur: " + this.getValeur();
	}

}
