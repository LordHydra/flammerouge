package application;
import java.util.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


/**
 * @author Vincent
 * @author Elouan
 *
 */
public class Jeu {
	private Parcours p;
	private ArrayList<Cycliste> listCycliste;
	private ArrayList<Joueur> ljoueurs;
	private Historique h; //objet qui va s'occuper d'�crires les stats au cours de la partie
	
	/**
	 * Constructeur par d�faut de la class Jeu
	 * @param p
	 * 		Parcours sur lequel on construit le jeu
	 * 
	 */
	public Jeu(Parcours p) {
		this.p=p;
		this.h = new Historique();
		this.listCycliste = new ArrayList<Cycliste>();
		this.ljoueurs = new ArrayList<Joueur>();
	}
	
	
	/**
	 * Initialise la partie
	 * 
	 */
	public void startGame() {
		Scanner sc = new Scanner(System.in);
		int nbJoueurs;
		while(true) {
			try {
				System.out.print("Entre le nombre de joueurs (2 � 4): ");
				nbJoueurs = sc.nextInt(); 
				while (nbJoueurs < 2 || nbJoueurs > 4) {
					System.out.println("Nombre de joueurs incorrect.");
					System.out.print("Entrez un autre nombre de joueurs (2 � 4): ");
					sc.nextLine();
					nbJoueurs = sc.nextInt();
				}
				break;
			}catch (InputMismatchException e) {
				System.out.println("Veuillez entrer un chiffre et non une lettre svp !");
				sc.next();
			}			
		}
		this.h.setNbJ(nbJoueurs);
		ArrayList<Joueur> l_joueurs = new ArrayList<Joueur>();
		String[][] positionDepart = new String[2][5];
		for (int i=0;i<positionDepart.length;i++) {
			for(int j=0;j<positionDepart[0].length;j++) {				
				positionDepart[i][j]="NN";
			}
		}
		for (int i = 0; i < nbJoueurs; i++) {
			System.out.print("Quel est votre nom Joueur" + (i + 1) + " ? ");
			String nomJ = sc.next();
			while (Outils.nomExistant(l_joueurs, nomJ)) {
				System.out.println("Ce nom d'�quipe � d�j� �t� attribu� veuillez en donner un autre");
				nomJ = sc.next();
			}
			Outils.afficherTab(positionDepart);
			int positionSprinteur;
			while(true){
				try {
					System.out.println("A quel position voulez vous placer votre Sprinter (entre 1 et 5) ? ");
					positionSprinteur = sc.nextInt();
					break;
				}catch (InputMismatchException e) {
					System.out.println("Veuillez entrer un chiffre et non une lettre svp !");
					sc.next();
				}					
			}
			Sprinter spr = new Sprinter(positionSprinteur,i+1);
			spr.remplirDeck();
			Outils.addCoordIntab(positionDepart, positionSprinteur,i+1, this.p, spr);
			
			Outils.afficherTab(positionDepart);
			int positionRouleur;
			while(true){
				try {
					System.out.println("A quel position voulez vous placer votre Rouleur (entre 1 et 5) ? ");
					positionRouleur = sc.nextInt();					
					break;
				}catch (InputMismatchException e) {
					System.out.println("Veuillez entrer un chiffre et non une lettre svp !");
					sc.next();
				}					
			}
			Rouleur roul = new Rouleur(positionRouleur,i+1);
			roul.remplirDeck();
			Outils.addCoordIntab(positionDepart, positionRouleur, i + 1, this.p,roul);
			l_joueurs.add(new Joueur(i+1, nomJ, spr, roul));
		}
		p.init_depart(positionDepart);
		String[] terrain = Outils.afficherTerrain(this.p);
		this.h.addStatusParcours(terrain);
		this.ljoueurs=l_joueurs;
		phaseEnergie();
	}

	
	
	
	/**
	 * Phase de pioche, les joueurs pioches et choisissent une carte � jouer.
	 */
	public void phaseEnergie() {
		Iterator<Joueur> i = this.ljoueurs.iterator();
		Scanner sc = new Scanner(System.in);
		while(i.hasNext()) {
			System.out.println("");
			Joueur j_courant = i.next();
			System.out.println("[Joueur :"+j_courant.getID()+"]");
			Rouleur r = j_courant.getRouleur();
			Sprinter s = j_courant.getSprinter();
			
			System.out.print("Dans quel paquet de cartes souhaitez vous piocher "+j_courant.getNom()+" ? (S pour sprinter ou R pour rouleur) :");
			String ch = sc.next();
			ch = ch.toUpperCase();
			while (!ch.equals("S") && !ch.equals("R")){
				System.out.println("");
				System.out.println("Veuillez �crire la lettre S pour choisir de piocher dans le paquet de carte de votre sprinter");
				System.out.println("Ou la lettre R pour choisir de piocher dans le paquet de carte de votre rouleur");
				System.out.println("Que choisissez vous ?");
				ch = sc.next();
				ch = ch.toUpperCase();
			}
			Carte jouer;
			Carte jouer2;
			if (ch.equals("S")) {
				jouer = Outils.selectCarteAjouerUtilsateur(s);
				s.setCarteJouer(jouer);
				System.out.println("Piochez dans votre deck rouleur � present");
				jouer2 = Outils.selectCarteAjouerUtilsateur(r);
				r.setCarteJouer(jouer2);
			}else {
				jouer = Outils.selectCarteAjouerUtilsateur(r);
				r.setCarteJouer(jouer);
				System.out.println("Piochez dans votre deck sprinter � present");
				jouer2 = Outils.selectCarteAjouerUtilsateur(s);
				s.setCarteJouer(jouer2);
			}
			this.h.addCarte(jouer);
			this.h.addCarte(jouer2);
		}
		phaseMouvement();
	}
	
	
	
	
	/**
	 * Phase permettant de faire avancer l'etat du jeu en fonction du relief du terrain.
	 * @param valueCarte
	 * 			Valeur d'une carte avant d'�tre trait� par la fonction
	 * @param c
	 * 		cycliste sur lequel la valuecarte va etre appliqu�
	 * @return valueCarte
	 * 		valeur v�rifi� et modifi� en fonction tu terrain
	 */
	public int seDeplacerEnFonctionDuTerrain(int valueCarte,Cycliste c) {
		//cas ou le cycliste est dans une mont�
		int positionDuCyclisteDansLeTab = c.getPosition()-1;
		int positionFuturDuCyclisteDansLeTab = c.getPosition()-1+valueCarte;
		if (this.p.estMontee(positionDuCyclisteDansLeTab)) {
			if (valueCarte > 5) {
				return 5;
			}
		}else if(this.p.estDescente(positionDuCyclisteDansLeTab)){
			if (valueCarte<5){
				return 5;
			}
		}else {
			//d�tection du d�but de la mont�e s'il y en a une 
			int i= positionDuCyclisteDansLeTab;
			while(!this.p.estMontee(i) && i<positionFuturDuCyclisteDansLeTab){
				i++;
			}
			int DistanceMonte = i-positionDuCyclisteDansLeTab;
			if (DistanceMonte != valueCarte) {
				if (DistanceMonte > 5) {
					return DistanceMonte-1;
				}else{
					if(valueCarte>5) {
						return 5;
					}
				}
			}else if (this.p.estMontee(positionFuturDuCyclisteDansLeTab) && valueCarte>5) {
				return 5;
			}
		}
		return valueCarte;
	}
	
	
	
	/**
	 * Phase dans laquelle l'etat du jeu avance
	 * 
	 */
	public void phaseMouvement() {
		Iterator<Joueur> i = this.ljoueurs.iterator();
		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		while(i.hasNext()) {
			Joueur j = i.next();
			lc.add(j.getRouleur());
			lc.add(j.getSprinter());
		}
		this.listCycliste =Outils.trierListCycliste(lc);
		Iterator<Cycliste> j = lc.iterator();
		String[][] posJoueurs = this.p.getStatusParcours();
		while(j.hasNext()) {
			Cycliste courant = j.next();
			
			int valueCarte = courant.getCarteJouer().getValeur();
			try {
				valueCarte = this.seDeplacerEnFonctionDuTerrain(valueCarte, courant);				
			}catch (ArrayIndexOutOfBoundsException e) {
				valueCarte = (this.p.getTaille()-courant.getPosition());
			}
			
			int posCycliste = courant.getPosition();
			while (!p.estLibredDroite(posCycliste+valueCarte) 
					&& !p.estLibredGauche(posCycliste+valueCarte) 
					&& valueCarte >=0) {
				valueCarte--;
			}
			courant.avancer(valueCarte);
							
			if (this.p.estLibredDroite(posCycliste+valueCarte)) {
				 courant.setEstDroite(true);
			}else {
				courant.setEstDroite(false);
			}
			
			this.p.updateSatusTab(lc);
		}
		this.listCycliste = Outils.trierListCycliste(lc);
		phaseFinal();
	}
	
	
	/**
	 * Applique fatigue � tous les cyclistes en t�te de groupe
	 * 
	 */
	public void appliquerFatigue() {
		ArrayList<ArrayList<Cycliste>> groupes = Outils.groupesCycliste(this.listCycliste);
		for(int i = 0; i < groupes.size(); i++) {
			if (groupes.get(i).size()>1) {
				if (groupes.get(i).get(0).getPosition() == groupes.get(i).get(1).getPosition()) {
					groupes.get(i).get(0).ajouterCarte(new CarteFatigue());
					groupes.get(i).get(1).ajouterCarte(new CarteFatigue());
				}else {
					groupes.get(i).get(0).ajouterCarte(new CarteFatigue());

				}
			}else {
				groupes.get(i).get(0).ajouterCarte(new CarteFatigue());
			}
		}
	}
	
	/**
	 * Methode permettant de gerer l'aspiration des cyclistes en fonction de leur position sur le circuit.
	 * 
	 */
	public void aspiration() {
		ArrayList<ArrayList<Cycliste>> groupes = Outils.groupesCycliste(this.listCycliste);
		if(groupes.size() > 1) {
			for (int i = groupes.size()-1;i>0;i--) {
				ArrayList<Cycliste> grpCourant = groupes.get(i);
				ArrayList<Cycliste> grpSuiv = groupes.get(i-1);
				int teteGroupeCourant = grpCourant.get(0).getPosition();
				int LastGroupeSuiv = grpSuiv.get(grpSuiv.size()-1).getPosition();
				if (LastGroupeSuiv-teteGroupeCourant == 2) {
					if (!this.p.estMontee(teteGroupeCourant-1) && !this.p.estMontee(LastGroupeSuiv-1)) {
						for (int j=0;j<grpCourant.size();j++) {
							grpCourant.get(j).avancer(1);
							grpSuiv.add(grpCourant.get(j));
						}
						groupes.remove(i);
						groupes.add(i,grpSuiv);
						this.p.updateSatusTab(this.listCycliste);					
					}
				}
			}
		}
	}
	
	/**
	 * Phase dans laquelle la fatigue est appliqu�e, ainsi que l'aspiration et check si la partie n'est pas termin�e.
	 */
	public void phaseFinal() {
		this.aspiration();
		String[] terrain2 = Outils.afficherTerrain(p);
		this.h.addStatusParcours(terrain2);
		this.appliquerFatigue();
		if (this.p.estArrive(this.listCycliste.get(0).getPosition()-1)) {
			System.out.println("La partie est termin�e !");
			int winner = this.listCycliste.get(0).getIdJoueur();
			for (int i=0;i<this.ljoueurs.size();i++) {
				if (this.ljoueurs.get(i).getID()==winner) {
					System.out.println("Le grand gagnant de la course est : "+this.ljoueurs.get(i).getNom());
				}
			}
			this.h.makeScoreBoard(this.listCycliste, this.ljoueurs);
			this.h.makeLogs(this.listCycliste);
		}else {
			phaseEnergie();			
		}
	}
	
	
	public void setListCylciste(ArrayList<Cycliste> lc) {
		this.listCycliste=lc;
	}
	
	public ArrayList<Cycliste> getListCylciste() {
		return this.listCycliste;
	}
	
}
	

