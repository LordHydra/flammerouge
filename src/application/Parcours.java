package application;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Vincent
 * @author Elouan
 *
 */
public class Parcours implements Serializable{
	private static final long serialVerionUID = 5118942035708796967L;
	String nom;
	int taille;	//longueur du parcours
	int[] relief;	//Tableau qui stock les mont�es et les descentes
	String[] terrain;	//Tableau qui stock les virages
	String[][] StatusParcours;	//Tableau qui stock la position des joueurs
	
	
	/**
	 * Constructeur de la classe Parcours
	 * @param longueur
	 * 					longueur du parcours souhait�e
	 * 
	 * @param nom
	 * 					nom du parcours
	 *
	 */
	public Parcours(int longueur,String nom){
		this.taille = longueur;
		this.nom = nom;
		this.terrain = new String[longueur];
		for (int i=0;i<this.terrain.length;i++) {
			this.terrain[i]="Plat";
		}
		this.relief = new int[longueur];
		this.StatusParcours = new String[2][longueur];
		for (int i=0;i<this.StatusParcours.length;i++) {
			for(int j=0;j<this.StatusParcours[0].length;j++) {				
				this.StatusParcours[i][j]="NN";
			}
		}
		this.ajouterLigneDepartArriver();
	}
	
	/**
	 * Methode qui permet d'ajouter la ligne de d�part et d'arriv�e
	 * 
	 * 
	 */
	public void ajouterLigneDepartArriver() {
		try{
			if (this.terrain.length<10) {
				throw new ParcoursException("Le parcours est trop petit");
			}
			for (int i=0;i<5;i++) {
				this.terrain[i]="Depart";
				this.terrain[this.terrain.length-i-1]="Arrive";
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe idx est une case d�part
	 * @param	
	 * 		idx : place ou l'on observe l'�tat 
	 * @return boolean
	 * */
	public boolean estDepart(int idx) {
		try {
			if (this.terrain[idx]=="Depart")return true;
			return false;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe idx est une case d�part 
	 *@param idx
	 * 		place ou l'on observe l'�tat
	 * @return boolean
	 * */
	public boolean estArrive(int idx) {
		try {
			if (this.terrain[idx]=="Arrive")return true;
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	/**
	 * Methode qui permet d'ajouter un virage
	 * @param serre
	 * 		boolean serre : permet de d�terminer si un virage est serre
	 * @param indiceDebut
	 * 		int indiceDebut : d�but du virage
	 * @param longueur
	 * 		int longueur : longueur du virage 
	 * 
	 * */
	public void addVirage(boolean serre,int indiceDebut,int longueur) {
		try{
			if (serre && longueur>4) {
				throw new ParcoursException("Votre virage est trop serre");
			}else if (this.estDepart(indiceDebut) || this.estArrive(indiceDebut+longueur-2)) {
				throw new ParcoursException("les lignes d'arriv�e ou de d�part ne peuvent �tre des virages");
			}else if (!serre && longueur>8){
				throw new ParcoursException("Votre virage est trop large");
			}
			for(int i =indiceDebut-1;i<=indiceDebut+longueur-2;i++) {
				if(serre) {
					this.terrain[i]="VirageSerre";
				}else {
					this.terrain[i]="VirageLeger";
				}
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Le virage que vous voulez inserer sort du parcours");
		}
	}
	
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe idx est un virage
	 *@param idx
	 * 		 place ou l'on observe l'�tat
	 * @return boolean
	 * */
	public boolean estVirage(int idx) {
		try {
			if (this.terrain[idx] == "VirageSerre" || this.terrain[idx] == "VirageLeger") return true;
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe idx est un virage Serre
	 * @param idx
	 * 		  place ou l'on observe l'�tat
	 * @return boolean
	 * */
	public boolean estVirageSerre(int idx) {
		try {
			if (this.estVirage(idx) && this.terrain[idx]=="VirageSerre")return true;
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe idx est un virage Leger
	 *  @param idx
	 * 		 place ou l'on observe l'�tat
	 * @return boolean
	 * */
	public boolean estVirageLeger(int idx) {
		try {
			if (this.estVirage(idx) && this.terrain[idx]=="VirageLeger")return true;
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	/**
	 * Methode qui permet d'ajouter du relief au parcour 
	 * @param indiceDebut
	 * 		int indiceDebut : d�part de la pente
	 * @param longueur
	 * 		int longueur : longueur de la pente
	 * @param typeRelief
	 * 		char typeRelief : indique quel type de pente la fonction va creer
	 * 
	 * */
	public void addRelief(int indiceDebut,int longueur,char typeRelief) {
		try{
			if (this.estDepart(indiceDebut) || this.estArrive(indiceDebut+longueur-2)) {
				throw new ParcoursException("les lignes d'arriv�e ou de d�part ne peuvent �tre une montagne");
			}
			for (int i=indiceDebut-1;i<indiceDebut+longueur-1;i++) {
				switch(typeRelief) {
				case 'M':
					this.relief[i]=1;
					break;
				case 'D':
					this.relief[i]=2;
					break;
				default:
					System.out.println("Erreur : type inconnu");
					break;
				}
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Le relief que vous voulez inserer sort du parcours");
		}
	}
	/**
	 * Methode permettant de d�terminer si une case � l'indexe idx est une mont�e
	 * @param idx
	 * 		 place ou l'on observe l'�tat
	 * @return boolean
	 * */
	public boolean estMontee(int idx) {
		try {
			if (this.relief[idx]==1)return true;
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Methode permettant de d�terminer si une case � l'indexe idx est une descente
	 * @param	idx
	 * 		place ou l'on observe l'�tat
	 * @return boolean
	 * */
	public boolean estDescente(int idx) {
		try {
			if (this.relief[idx]==2)return true;
			return false;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	
	/**
	 * Methode qui permet d'ajouter une montee puis une descente ou l'inverse
	 * @param indiceDebut
	 * 		int indiceDebut : d�part de la pente
	 * @param monterFirst
	 * 		boolean monterFirst : boolean qui indique si c'est la monter en premier ou la descente
	 * @param longueur1
	 * 		int longueur1 : longueur de la premi�re pente
	 * @param longueur2
	 * 		int longueur2 : longueur de la seconde pente
	 * 		
	 * */
	public void addDune(int indiceDebut,boolean monterFirst,int longueur1,int longueur2) {
		try{
			if (this.estDepart(indiceDebut) || this.estArrive(indiceDebut+longueur1+longueur2)) {
				throw new ParcoursException("les lignes d'arriv�e ou de d�part ne peuvent �tre une montagne");
			}
			for (int i=indiceDebut-1;i<indiceDebut+longueur1-1;i++) {
				if (monterFirst) {
					this.relief[i]=1;
				}else {
					this.relief[i]=2;
				}
			}
			for (int i=indiceDebut+longueur1-1;i<indiceDebut+longueur1+longueur2-1;i++) {
				if (monterFirst) {
					this.relief[i]=2;
				}else {
					this.relief[i]=1;
				}
			}
		}catch (ParcoursException e) {
			e.printStackTrace();
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Le relief que vous voulez inserer sort du parcours");
		}
	}
	
	/**
	 * Methode qui initialise les places des joueurs dans le parcous
	 * @param tab
	 * 		int[][] tab : tableau qui repr�sente la composition de la zone de d�part (emplacement des joueurs)
	 */
	public void init_depart(String[][] tab) {
		for (int i=0;i<tab.length;i++) {
			for (int j=0;j<tab[0].length;j++) {
				this.StatusParcours[i][j]=tab[i][j];
			}
		}
	}
	
	/**
	 *Methode qui renvoie un boolean indiquant si la case est libre sur sa partie droite
	 *@param idx
	 *			indice de la case sur laquelle faire le test
	 *
	 *@return true si emplacement libre
	 */
	public boolean estLibredDroite(int idx) {
		try {
			if (this.StatusParcours[1][idx-1]=="NN") return true;
			return false;			
		}catch (Exception e) {
			return false;
		}
	}
	
	/**
	 *Methode qui renvoie un boolean indiquant si la case est libre sur sa partie gauche
	 *@param idx
	 *			indice de la case sur laquelle faire le test
	 *
	 *@return true si emplacement libre
	 *
	 */
	public boolean estLibredGauche(int idx) {
		try {
			if (this.StatusParcours[0][idx-1]=="NN") return true;
			return false;
		}catch (Exception e) {
			return false;
		}
	}
	
	
	/**
	 *Methode qui permet de refresh l'affichage du circuit dans la console
	 *@param lc
	 *			liste des pions sur le plateau
	 *
	 */
	public void updateSatusTab(ArrayList<Cycliste> lc) {
		for (int i=0;i<this.StatusParcours.length;i++) {
			for(int j=0;j<this.StatusParcours[0].length;j++) {				
				this.StatusParcours[i][j]="NN";
			}
		}
		for (int i=0;i<lc.size();i++) {
			Cycliste courant = lc.get(i);
			if (lc.get(i).getEstAdroite()) {
				this.StatusParcours[1][courant.getPosition()-1]=courant.quelestMonType()+courant.getIdJoueur();
			}else {
				this.StatusParcours[0][courant.getPosition()-1]=courant.quelestMonType()+courant.getIdJoueur();
			}
		}
	}
	
	
	/*
	 * Getters
	 */
	
	public int[] getRelief() {
		return this.relief;
	}
	
	public String[][] getStatusParcours() {
		return this.StatusParcours;
	}
	
	public String[] getTerrain(){
		return this.terrain;
	}
	
	public int getTaille() {
		return this.taille;
	}
	
	/**
	 * Methode qui afficher l'�tat d'un objet parcours 
	 */
	public String toString() {
		String ligne="",ligne2="",relief="",terrain = "";
		for (int i=0;i<this.StatusParcours[0].length;i++) {
			ligne +=this.StatusParcours[0][i]+" ";
			ligne2 +=this.StatusParcours[1][i]+" ";
			relief +=this.relief[i]+" ";
			terrain +=this.terrain[i]+" ";
		}
		return ligne+"\n"+ligne2+"\n"+terrain+"\n"+relief+"\n";
	}
	
}