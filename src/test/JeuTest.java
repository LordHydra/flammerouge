package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import application.CarteEnergie;
import application.*;

/**
 * @author Elouan
 *@author Vincent
 */
public class JeuTest {
	
	@Test
	public void testSeDeplacerEnfonctionDuTerrain() {
		Parcours p = new Parcours(40,"test");
		p.addDune(10, true, 10, 10);
		Jeu j = new Jeu(p);
		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(new Rouleur(12,1));
		lc.add(new Sprinter(4,1));
		lc.add(new Sprinter(22,2));
		lc.add(new Rouleur(1,2));
		lc.add(new Rouleur(8,3));
		assertEquals("R1 devrait avancer de 5",5,j.seDeplacerEnFonctionDuTerrain(9,lc.get(0)));
		assertEquals("S1 devrait avancer de 5",5,j.seDeplacerEnFonctionDuTerrain(9,lc.get(1)));
		assertEquals("S2 devrait avancer de 5",5,j.seDeplacerEnFonctionDuTerrain(2,lc.get(2)));
		assertEquals("R2 devrait avancer de 7",7,j.seDeplacerEnFonctionDuTerrain(7,lc.get(3)));
		assertEquals("R3 devrait avancer de 5",5,j.seDeplacerEnFonctionDuTerrain(7,lc.get(4)));

		
	}
	
	@Test
	public void testFatigue() {
		Jeu j = new Jeu(new Parcours(40,"test"));
		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(new Rouleur(5,1));
		lc.add(new Sprinter(13,1));
		lc.add(new Sprinter(14,2));
		lc.add(new Rouleur(14,2));
		lc.add(new Sprinter(15,3));
		lc.add(new Rouleur(15,3));
		Collections.reverse(lc);
		j.setListCylciste(lc);
		j.appliquerFatigue();
		assertEquals("R3 devrait avoir une carte Fatigue",1,j.getListCylciste().get(0).getCarteVisible().size());
		assertEquals("S3 devrait avoir une carte Fatigue",1,j.getListCylciste().get(1).getCarteVisible().size());
		assertEquals("R2 devrait avoir 0 carte Fatigue",0,j.getListCylciste().get(2).getCarteVisible().size());
		assertEquals("S2 devrait avoir 0 carte Fatigue",0,j.getListCylciste().get(3).getCarteVisible().size());
		assertEquals("S1 devrait avoir 0 carte Fatigue",0,j.getListCylciste().get(4).getCarteVisible().size());
		assertEquals("R1 devrait avoir une carte Fatigue",1,j.getListCylciste().get(5).getCarteVisible().size());
	}
	
	@Test
	public void testAspiration() {
		Jeu j = new Jeu(new Parcours(40,"test"));
		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(new Rouleur(5,1));
		lc.add(new Sprinter(7,1));
		lc.add(new Sprinter(9,2));
		lc.add(new Rouleur(11,2));
		lc.add(new Sprinter(11,3));
		lc.add(new Rouleur(13,3));
		Collections.reverse(lc);
		j.setListCylciste(lc);
		j.aspiration();		
		assertEquals("R3 devrait etre en position 13",13,j.getListCylciste().get(0).getPosition());
		assertEquals("S3 devrait etre en position 12",12,j.getListCylciste().get(1).getPosition());
		assertEquals("R2 devrait etre en position 12",12,j.getListCylciste().get(2).getPosition());
		assertEquals("S2 devrait etre en position 11",11,j.getListCylciste().get(3).getPosition());
		assertEquals("S1 devrait etre en position 10",10,j.getListCylciste().get(4).getPosition());
		assertEquals("R1 devrait etre en position 9",9,j.getListCylciste().get(5).getPosition());
	}
	
	@Test
	public void testAspirationEnMontee() {
		Parcours p = new Parcours(40,"test");
		p.addRelief(7,11,'M');
		Jeu j = new Jeu(p);
		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		lc.add(new Rouleur(5,1));
		lc.add(new Sprinter(7,1));
		lc.add(new Sprinter(9,2));
		lc.add(new Rouleur(11,2));
		lc.add(new Sprinter(11,3));
		lc.add(new Rouleur(13,3));
		Collections.reverse(lc);
		j.setListCylciste(lc);
		j.aspiration();		
		assertEquals("R3 devrait etre inchang�",13,j.getListCylciste().get(0).getPosition());
		assertEquals("S3 devrait etre inchang�",11,j.getListCylciste().get(1).getPosition());
		assertEquals("R2 devrait etre inchang�",11,j.getListCylciste().get(2).getPosition());
		assertEquals("S2 devrait etre inchang�",9,j.getListCylciste().get(3).getPosition());
		assertEquals("S1 devrait etre inchang�",7,j.getListCylciste().get(4).getPosition());
		assertEquals("R1 devrait etre inchang�",5,j.getListCylciste().get(5).getPosition());
	}

}
